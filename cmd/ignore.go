package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/madflow/labfriend/core"
	"github.com/madflow/labfriend/display"
	prompt "github.com/madflow/labfriend/prompt/ignore"
	"github.com/spf13/cobra"
)

func writeFile(data []byte) {
	err := os.WriteFile(".gitignore", data, 0o600)

	if err != nil {
		display.AlertSuccess(fmt.Sprintf("A file writing error occured %v", err))
		os.Exit(1)
	}
	display.AlertSuccess(".gitignore written")
}

var ignoreCmd = &cobra.Command{
	Use:   "ignore",
	Short: "Get a gitignore template",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {

		keyFlag, _ := cmd.Flags().GetString("key")
		git := core.CreateApiClient()

		ignore, _, err := git.GitIgnoreTemplates.GetTemplate(keyFlag)
		if err != nil {
			result := prompt.Prompt()

			ignore, _, err := git.GitIgnoreTemplates.GetTemplate(result)

			if err != nil {
				log.Fatalf("Failed to get the template")
			}

			writeFile([]byte(ignore.Content))
			os.Exit(0)
		}
		writeFile([]byte(ignore.Content))

	},
}

func init() {
	rootCmd.AddCommand(ignoreCmd)
	ignoreCmd.Flags().StringP("key", "k", "", "The key of the .gitignore template")
}
