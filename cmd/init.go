package cmd

import (
	prompt "github.com/madflow/labfriend/prompt/init"
	"github.com/spf13/cobra"
)

var initCmd = &cobra.Command{
	Use:   "init",
	Short: "Create a new config file.",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		prompt.Start()
	},
}

func init() {
	rootCmd.AddCommand(initCmd)
}
