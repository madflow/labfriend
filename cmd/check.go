package cmd

import (
	"os"

	"github.com/madflow/labfriend/core"
	"github.com/madflow/labfriend/display"
	"github.com/spf13/cobra"
)

func checkRun(cmd *cobra.Command, args []string) {
	git := core.CreateApiClient()

	version, _, err := git.Version.GetVersion()

	if err != nil {
		display.AlertError(err.Error())
		display.AlertError("Your setup is not properly configured. Please use the 'lab init' command to configure your Gitlab instance.")
		os.Exit(0)
	}

	display.AlertSuccess("Success")
	output := [2]string{git.BaseURL().String(), version.Version}
	display.BulletedParagraphs(output[:])
}

var checkCmd = &cobra.Command{
	Use:   "check",
	Short: "Check if everything is peachy and the Gitlab API can be reached.",
	Run:   checkRun,
}

func init() {
	rootCmd.AddCommand(checkCmd)
}
