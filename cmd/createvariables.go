package cmd

import (
	"fmt"
	"log"

	"github.com/madflow/labfriend/core"
	"github.com/madflow/labfriend/display"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	"gopkg.in/ini.v1"
)

var createvariablesCmd = &cobra.Command{
	Use:   "createvariables",
	Short: "Create project variables from an ini file",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		projectFlag, _ := cmd.Flags().GetString("project")
		iniFlag, _ := cmd.Flags().GetString("ini")

		git := core.CreateApiClient()

		cfg, err := ini.Load(iniFlag)
		if err != nil {
			log.Fatalf("Failed to read file: %v", err)
		}

		sec, _ := cfg.GetSection(ini.DefaultSection)
		keys := sec.KeyStrings()

		for i := 0; i < len(keys); i++ {
			key := keys[i]
			value := sec.Key(key).String()

			variable, _, err := git.ProjectVariables.CreateVariable(projectFlag, &gitlab.CreateProjectVariableOptions{
				Key:   &key,
				Value: &value,
			})

			if err != nil {
				display.BulletedParagraph(fmt.Sprintf("Failed to create variable: %v", err))
			} else {
				display.BulletedParagraph(fmt.Sprintf("Create variable: %q", variable))
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(createvariablesCmd)
	createvariablesCmd.Flags().StringP("project", "p", "", "NAMESPACE/PROJECT_NAME of the project or the project id")
	createvariablesCmd.Flags().StringP("ini", "i", "", "The ini file")
	createvariablesCmd.MarkFlagRequired("project")
	createvariablesCmd.MarkFlagRequired("ini")

}
