package cmd

import (
	"fmt"
	"os"

	"github.com/madflow/labfriend/core"
	"github.com/madflow/labfriend/display"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string

var rootCmd = &cobra.Command{
	Use:   "lab",
	Short: "Your Gitlab friend",
	Long:  ``,
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.labfriend.yaml)")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		if !core.FileExists(cfgFile) {
			display.AlertError(fmt.Sprintf("The provided config file %q does not exist.", cfgFile))
			os.Exit(0)
		}
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Find current directory.
		current, err := os.Getwd()
		cobra.CheckErr(err)

		viper.AddConfigPath(home)
		viper.AddConfigPath(current)
		viper.SetConfigType("yaml")
		viper.SetConfigName(".labfriend")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		display.AlertMuted(fmt.Sprintf("Using config file %q.", viper.ConfigFileUsed()))
	}
}
