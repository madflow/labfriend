package cmd

import (
	"log"

	"github.com/madflow/labfriend/core"
	"github.com/madflow/labfriend/display"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	"gopkg.in/ini.v1"
)

var dumpvariablesCmd = &cobra.Command{
	Use:   "dumpvariables",
	Short: "Dump project variables and save them to an ini file",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		projectFlag, _ := cmd.Flags().GetString("project")
		iniFlag, _ := cmd.Flags().GetString("ini")
		git := core.CreateApiClient()

		variables, _, err := git.ProjectVariables.ListVariables(projectFlag, &gitlab.ListProjectVariablesOptions{
			PerPage: 200,
		})

		if err != nil {
			log.Fatalf("Failed to fetch variables: %v", err)
		}

		ini.PrettyFormat = false
		cfg := ini.Empty()

		sec := cfg.Section("")

		for i := 0; i < len(variables); i++ {
			variable := variables[i]
			key := variable.Key
			value := variable.Value
			sec.NewKey(key, value)
		}

		err = cfg.SaveToIndent(iniFlag, "")

		if err != nil {
			log.Fatalf("Failed to write ini file:: %v", err)
		}

		display.AlertSuccess("Project variables written to " + iniFlag)

	},
}

func init() {
	rootCmd.AddCommand(dumpvariablesCmd)
	dumpvariablesCmd.Flags().StringP("project", "p", "", "NAMESPACE/PROJECT_NAME of the project or the project id")
	dumpvariablesCmd.Flags().StringP("ini", "i", "", "The output ini file")
	dumpvariablesCmd.MarkFlagRequired("project")
	dumpvariablesCmd.MarkFlagRequired("ini")
}
