package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/madflow/labfriend/core"
	"github.com/madflow/labfriend/display"
	"github.com/spf13/cobra"
)

var lintCmd = &cobra.Command{
	Use:   "lint",
	Short: "Checks if a CI/CD YAML configuration is valid",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {

		git := core.CreateApiClient()

		fileFlag, _ := cmd.Flags().GetString("file")

		if !core.FileExists(fileFlag) {
			display.AlertError(fmt.Sprintf("The provided file %q for linting does not exist.", fileFlag))
			os.Exit(0)
		}

		yfile, errRead := os.ReadFile(fileFlag)

		if errRead != nil {
			log.Fatal(errRead)
		}

		s := string(yfile)

		validate, _, errLint := git.Validate.Lint(s)

		if errLint != nil {
			log.Fatal(errLint)
		}

		if validate.Status == "valid" {
			display.AlertSuccess(fmt.Sprintf("The linted file %q is valid.", fileFlag))
		} else {
			display.AlertError(fmt.Sprintf("The linted file %q is not valid.", fileFlag))
			if len(validate.Warnings) > 0 {
				display.BulletedParagraphs(validate.Warnings)
			}
			if len(validate.Errors) > 0 {
				display.BulletedParagraphs(validate.Errors)
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(lintCmd)
	lintCmd.Flags().StringP("file", "f", "", "The local .gitlab-ci.yml file")
	lintCmd.MarkFlagRequired("file")
}
