# Labfriend - your best GitLab Buddy

A series of friendly commands for everyday GitLab joy.

```lab -h```

```
Your Gitlab friend

Usage:
  lab [command]

Available Commands:
  check           Check if everything is peachy and the Gitlab API can be reached.
  completion      Generate the autocompletion script for the specified shell
  createvariables Create project variables from an ini file
  dumpvariables   Dump project variables and save them to an ini file
  help            Help about any command
  ignore          Get a gitignore template
  init            Create a new config file.
  lint            Checks if a CI/CD YAML configuration is valid

Flags:
      --config string   config file (default is $HOME/.labfriend.yaml)
  -h, --help            help for lab

Use "lab [command] --help" for more information about a command.
```

## Installation

### Binaries for Linux, macOS and Windows

* https://gitlab.com/madflow/labfriend/-/releases

### Source code

* Clone this repository
* Run `go build`
* Copy the `labfriend` binary to your PATH




