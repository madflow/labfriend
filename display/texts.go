package display

import (
	"fmt"

	"github.com/charmbracelet/lipgloss"
)

var paragraphStyle = lipgloss.NewStyle().
	Width(80)

func Paragraph(text string) {
	fmt.Println(paragraphStyle.Render(text))
}

func BulletedParagraph(text string) {
	fmt.Println(paragraphStyle.Render("• " + text))
}

func Paragraphs(texts []string) {
	for _, str := range texts {
		Paragraph(str)
	}
}

func BulletedParagraphs(texts []string) {
	Divider()
	for _, str := range texts {
		BulletedParagraph(str)
	}
	Divider()
}
