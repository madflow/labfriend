package display

import (
	"fmt"

	"github.com/charmbracelet/lipgloss"
)

var dividerStyle = lipgloss.NewStyle().
	MarginTop(1).
	Width(80)

func Divider() {
	fmt.Println(dividerStyle.Render(""))
}
