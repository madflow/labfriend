package display

import (
	"fmt"

	"github.com/charmbracelet/lipgloss"
	"github.com/madflow/labfriend/theme"
)

var successStyle = lipgloss.NewStyle().
	Bold(true).
	Foreground(lipgloss.Color(theme.Colors.Blue)).
	Background(lipgloss.Color(theme.Colors.Indigo)).
	Padding(1).
	Width(80)

var errorStyle = lipgloss.NewStyle().
	Bold(true).
	Foreground(lipgloss.Color(theme.Colors.Yellow)).
	Background(lipgloss.Color(theme.Colors.Red)).
	Padding(1).
	Width(80)

var warningStyle = lipgloss.NewStyle().
	Bold(true).
	Foreground(lipgloss.Color(theme.Colors.Red)).
	Background(lipgloss.Color(theme.Colors.Yellow)).
	Padding(1).
	Width(80)

var mutedStyle = lipgloss.NewStyle().
	Bold(true).
	Foreground(lipgloss.Color(theme.Grays.LightGray)).
	Padding(1).
	Width(80)

func AlertSuccess(message string) {
	fmt.Println(successStyle.Render(message))
}

func AlertError(message string) {
	fmt.Println(errorStyle.Render(message))
}

func AlertWarning(message string) {
	fmt.Println(warningStyle.Render(message))
}

func AlertMuted(message string) {
	fmt.Println(mutedStyle.Render(message))
}
