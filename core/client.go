package core

import (
	"log"

	"github.com/spf13/viper"
	"github.com/xanzy/go-gitlab"
)

func CreateApiClient() *gitlab.Client {
	var apiKey string = viper.GetString("api_key")
	var apiUrl string = viper.GetString("api_url")

	git, err := gitlab.NewClient(apiKey, gitlab.WithBaseURL(apiUrl))

	if err != nil {
		log.Fatalf("Failed to create gitlab client: %v", err)
	}

	return git
}
