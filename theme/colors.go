package theme

type ColorNames struct {
	Blue   string
	Indigo string
	Purple string
	Pink   string
	Red    string
	Orange string
	Yellow string
	Green  string
	Teal   string
	Cyan   string
}

type GrayColorNames struct {
	Black     string
	White     string
	LightGray string
	Gray      string
	DarkGray  string
}

var Colors = ColorNames{
	Blue:   "0d6efd",
	Indigo: "#6610f2",
	Pink:   "#d63384",
	Red:    "#dc3545",
	Orange: "#fd7e14",
	Yellow: "#ffc107",
	Green:  "#198754",
	Teal:   "#20c997",
	Cyan:   "#0dcaf0",
}

var Grays = GrayColorNames{
	Black:     "#000000",
	White:     "#ffffff",
	LightGray: "#dee2e6",
	Gray:      "#6c757d",
	DarkGray:  "#212529",
}
